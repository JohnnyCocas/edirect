const mariadb = require('mariadb');

const pool = mariadb.createPool({host: '127.0.0.1', port: 3306, database: 'edirect', user: 'root', password: 'root', multipleStatements: true});

function sendQuery(str, params) {
  return new Promise((res, rej) => {
    pool.getConnection().then(conn => {
      conn.query(str, params).then(rows => {
        conn.release();
        res(rows);
      }).catch(err => {
        console.log("not connected due to error: " + err);
        rej({ error: err });
      }).finally(() => {
        conn.release();
      });
    }).catch(err => {
      console.log("not connected due to error: " + err);
      rej({ error: err });
    });
  });
}

module.exports = sendQuery;
