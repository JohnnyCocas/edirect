const express = require('express');
const router = express.Router();
const sendQuery = require('../connector/connector');

router.param('id', function (req, res, next, id) {
    req.projectId = id;
    next();
})
router.param('taskId', function (req, res, next, id) {
    req.taskId = id;
    next();
})

router.route('/').all(function (req, res, next) {
    next();
}).get(function (req, res, next) {
    sendQuery("SELECT * FROM project WHERE user=?", [req.headers.userid]).then(data => {
        if (data.length) {
            let query = '';
            let queryParams = [];

            /* There is almost certainly a better way to do this, but I don't remember nor can find it */
            data.forEach(project => {
                query += "SELECT * FROM task WHERE task.projectId=?;";
                queryParams.push(project.id);
            });

            sendQuery(query, queryParams).then(taskData => {
                queryParams.forEach((projectId, idx) => {
                    const project = data.find(proj => proj.id === projectId);
                    if (project) {
                        project.tasks = queryParams.length === 1 ? taskData || [] : taskData[idx] || [];
                    }
                });
                res.json(data);
            }).catch(err => {
                next(err);
            });
        } else {
            res.json([]);
        }
    }).catch(err => {
        next(err);
    });
}).post(function (req, res, next) {
    sendQuery("INSERT INTO project (name, user) VALUES (?, ?); SELECT * FROM project WHERE user=?", [req.body.name, req.headers.userid, req.headers.userid]).then(data => {
        if (data[1]) {
            let query = '';
            let queryParams = [];

            data[1].forEach(project => {
                query += "SELECT * FROM task WHERE task.projectId=?;";
                queryParams.push(project.id);
            });

            sendQuery(query, queryParams).then(taskData => {
                queryParams.forEach((projectId, idx) => {
                    const project = data[1].find(proj => proj.id === projectId);
                    if (project) {
                        project.tasks = queryParams.length === 1 ? taskData || [] : taskData[idx] || [];
                    }
                });
                res.json(data[1]);
            }).catch(err => {
                next(err);
            });
        } else {
            res.json([]);
        }
    }).catch(err => {
        next(err);
    });
})

router.route('/:id').all(function (req, res, next) {
    next();
}).get(function (req, res, next) {
    sendQuery("SELECT * FROM project WHERE id=?", [req.projectId]).then(data => {
        if (data.length) {
            sendQuery("SELECT * FROM task WHERE task.projectId=?", [req.projectId]).then(taskData => {
                if (taskData.length) {
                    data[0].tasks = taskData;
                    res.json(data[0]);
                } else {
                    next(new Error('No tasks found'));
                }
            }).catch(err => {
                next(err);
            });
        } else {
            next(new Error('Project not found'));
        }
    }).catch(err => {
        next(err);
    });
}).post(function (req, res, next) {
    sendQuery("INSERT INTO task (name, projectId, finishDate, finished) VALUES (?, ?, ?, 0); SELECT * FROM project WHERE id=?", [req.body.name, req.projectId, req.body.finishDate || null, req.projectId]).then(data => {
        if (data[1] && data[1][0]) {
            sendQuery("SELECT * FROM task WHERE task.projectId=?", [req.projectId]).then(taskData => {
                if (taskData.length) {
                    data[1][0].tasks = taskData;
                    res.json(data[1][0]);
                } else {
                    next(new Error('No tasks found'));
                }
            }).catch(err => {
                next(err);
            });
        } else {
            next(new Error('Project not found'));
        }
    }).catch(err => {
        next(err);
    });
}).put(function (req, res, next) {
    sendQuery("UPDATE project SET name=? WHERE id=?; SELECT * FROM project WHERE id=?", [req.body.name, req.projectId, req.projectId]).then(data => {
        if (data[1] && data[1][0]) {
            sendQuery("SELECT * FROM task WHERE task.projectId=?", [req.projectId]).then(taskData => {
                data[1][0].tasks = taskData || [];
                res.json(data[1][0]);
            }).catch(err => {
                next(err);
            });
        } else {
            next(new Error('Project not found'));
        }
    }).catch(err => {
        next(err);
    });
}).delete(function (req, res, next) {
    sendQuery("DELETE FROM project WHERE id=?; SELECT * FROM project WHERE user=?", [req.projectId, req.headers.userid]).then(data => {
        if (data[1].length) {
            let query = '';
            let queryParams = [];

            data[1].forEach(project => {
                query += "SELECT * FROM task WHERE task.projectId=?;";
                queryParams.push(project.id);
            });

            sendQuery(query, queryParams).then(taskData => {
                queryParams.forEach((projectId, idx) => {
                    const project = data[1].find(proj => proj.id === projectId);
                    if (project) {
                        project.tasks = queryParams.length === 1 ? taskData || [] : taskData[idx] || [];
                    }
                });
                res.json(data[1]);
            }).catch(err => {
                next(err);
            });
        } else {
            res.json([]);
        }
    }).catch(err => {
        next(err);
    });
})

router.route('/:id/task/:taskId').all(function (req, res, next) {
    next();
}).get(function (req, res, next) {
    sendQuery("UPDATE task SET finished=IF(finished>0,0,1) WHERE id=?; SELECT * FROM project WHERE id=?", [req.taskId, req.projectId]).then(data => {
        if (data[1] && data[1][0]) {
            sendQuery("SELECT * FROM task WHERE task.projectId=?", [req.projectId]).then(taskData => {
                data[1][0].tasks = taskData || [];
                res.json(data[1][0]);
            }).catch(err => {
                next(err);
            });
        } else {
            next(new Error('Project not found'));
        }
    }).catch(err => {
        next(err);
    });
}).put(function (req, res, next) {
    sendQuery("UPDATE task SET name=?, finishDate=? WHERE id = ?; SELECT * FROM project WHERE id=?", [req.body.name, req.body.finishDate || null, req.taskId, req.projectId]).then(data => {
        if (data[1] && data[1][0]) {
            sendQuery("SELECT * FROM task WHERE task.projectId=?", [req.projectId]).then(taskData => {
                data[1][0].tasks = taskData || [];
                res.json(data[1][0]);
            }).catch(err => {
                next(err);
            });
        } else {
            next(new Error('Project not found'));
        }
    }).catch(err => {
        next(err);
    });
}).delete(function (req, res, next) {
    sendQuery("DELETE FROM task WHERE id=?; SELECT * FROM project WHERE id=?", [req.taskId, req.projectId]).then(data => {
        if (data[1] && data[1][0]) {
            sendQuery("SELECT * FROM task WHERE task.projectId=?", [req.projectId]).then(taskData => {
                data[1][0].tasks = taskData || [];
                res.json(data[1][0]);
            }).catch(err => {
                next(err);
            });
        } else {
            next(new Error('Project not found'));
        }
    }).catch(err => {
        next(err);
    });
})

// TODO: Remove all the duplicate code

module.exports = router;
