const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const sendQuery = require('../connector/connector');
const saltRounds = 10;

/* Login. */
router.route('/').all(function (req, res, next) {
    next();
}).get(function (req, res, next) {
    sendQuery("SELECT * FROM user WHERE username=?", [req.headers.username]).then(data => {
        if (data.length) {
            req.user = data[0];
            req.hash = req.user.password;

            bcrypt.compare(req.headers.password, req.hash, function(err, result) {
                if (result) {
                    res.json(req.user);
                } else {
                    next(new Error('Invalid Credentials'));
                }
            });
        } else {
            next(new Error('Invalid Credentials'));
        }
    }).catch(err => {
        next(err);
    });
});

/* Register. */
router.route('/register').all(function (req, res, next) {
    next();
}).post(function (req, res, next) {
    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
        sendQuery("INSERT INTO user (username, name, password) VALUES (?, ?, ?); SELECT * FROM user WHERE username=?", [req.body.username, req.body.name, hash, req.body.username]).then(data => {
            if (data[1] && data[1][0]) {
                res.json(data[1][0]);
            } else {
                next(new Error('Invalid Credentials'));
            }
        }).catch(err => {
            next(err);
        }).finally(() => {
            res.end();
        })
    });
});

// TODO: Generate token for session management

module.exports = router;
