import { TestBed, inject } from '@angular/core/testing';

import { MockAuthenticationService } from '@app/auth/authentication.service.mock';
import { ShellComponent } from './shell.component';
import { Shell } from './shell.service';
import { AuthenticationGuard } from '@app/auth/authentication.guard';
import { AuthenticationService } from '@app/auth/authentication.service';

describe('Shell', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ShellComponent],
      providers: [AuthenticationGuard, { provide: AuthenticationService, useClass: MockAuthenticationService }],
    });
  });

  describe('childRoutes', () => {
    it('should create routes as children of shell', () => {
      // Prepare
      const testRoutes = [{ path: 'test' }];

      // Act
      const result = Shell.childRoutes(testRoutes);

      // Assert
      expect(result.path).toBe('');
      expect(result.children).toBe(testRoutes);
      expect(result.component).toBe(ShellComponent);
    });
  });
});
