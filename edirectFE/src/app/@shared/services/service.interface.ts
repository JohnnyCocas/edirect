export interface Project {
  id: string;
  name: string;
  tasks: Task[];
}

export interface Task {
  id: string;
  finished: boolean;
  name: string;
  creationDate: string;
  finishDate: string;
}
