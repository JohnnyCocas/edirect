import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { Project } from '@shared/services/service.interface';
import { Credentials, CredentialsService } from '@app/auth/credentials.service';
import { map } from 'rxjs/operators';

export interface ProjectContext {
  name: string;
}

export interface TaskContext {
  name: string;
  finishDate: string;
}

@Injectable()
export class TaskService {

  constructor(
    private http: HttpClient,
    private credentialsService: CredentialsService
  ) {}

  getProjectList(filters: {[key: string]: string;}[] = []): Observable<Project[]> {

    let params = new HttpParams();
    //params = params.set('id', id);

    let headers = new HttpHeaders({
      userid: this.credentialsService.credentials.username
    });

    if (filters) {
      Object.keys(filters).forEach((key: string) => {
        params = params.set('filter.' + key, filters[key]);
      });
    }

    return this.http.get<Project[]>(environment.routes.getProjectList(), { params: params, headers: headers }).pipe(
      map((projects: Project[]) => {
        projects?.map(p => p.tasks?.map(t => t.finished = !!t.finished));
        return projects;
      })
    );
  }

  getTaskList(projectId: string, filters: {[key: string]: string;}[] = []): Observable<Project> {

    let params = new HttpParams();

    //params = params.set('id', id);

    let headers = new HttpHeaders({
      // id: id,
    });

    if (filters) {
      Object.keys(filters).forEach((key: string) => {
        params = params.set('filter.' + key, filters[key]);
      });
    }

    return this.http.get<Project>(environment.routes.getTaskList(projectId), { params: params, headers: headers }).pipe(
      map((project: Project) => {
        project?.tasks?.map(t => t.finished = !!t.finished);
        return project;
      })
    );
  }

  createProject(project: ProjectContext): Observable<Project[]> {
    let headers = new HttpHeaders({
      userid: this.credentialsService.credentials.username
    });

    return this.http.post<Project[]>(environment.routes.createProject(), project, { headers: headers }).pipe(
      map((projects: Project[]) => {
        projects?.map(p => p.tasks?.map(t => t.finished = !!t.finished));
        return projects;
      })
    );
  }

  editProject(project: ProjectContext, projectId: string): Observable<Project> {
    return this.http.put<Project>(environment.routes.editProject(projectId), project).pipe(
      map((project: Project) => {
        project?.tasks?.map(t => t.finished = !!t.finished);
        return project;
      })
    );
  }

  deleteProject(projectId: string): Observable<Project[]> {
    let headers = new HttpHeaders({
      userid: this.credentialsService.credentials.username
    });

    return this.http.delete<Project[]>(environment.routes.deleteProject(projectId), { headers: headers }).pipe(
      map((projects: Project[]) => {
        projects?.map(p => p.tasks?.map(t => t.finished = !!t.finished));
        return projects;
      })
    );
  }

  createTask(task: TaskContext, projectId: string): Observable<Project> {
    return this.http.post<Project>(environment.routes.createTask(projectId), task).pipe(
      map((project: Project) => {
        project?.tasks?.map(t => t.finished = !!t.finished);
        return project;
      })
    );
  }

  editTask(task: TaskContext, taskId: string, projectId: string): Observable<Project> {
    return this.http.put<Project>(environment.routes.editTask(projectId, taskId), task).pipe(
      map((project: Project) => {
        project?.tasks?.map(t => t.finished = !!t.finished);
        return project;
      })
    );
  }

  toggleTask(taskId: string, projectId: string): Observable<Project> {
    return this.http.get<Project>(environment.routes.toggleTask(projectId, taskId)).pipe(
      map((project: Project) => {
        project?.tasks?.map(t => t.finished = !!t.finished);
        return project;
      })
    );
  }

  deleteTask(taskId: string, projectId: string): Observable<Project> {
    return this.http.delete<Project>(environment.routes.deleteTask(projectId, taskId)).pipe(
      map((project: Project) => {
        project?.tasks?.map(t => t.finished = !!t.finished);
        return project;
      })
    );
  }
}
