import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { LoaderComponent } from './loader/loader.component';
import { TaskService } from '@shared/services/task.service';

@NgModule({
  imports: [FlexLayoutModule, MaterialModule, CommonModule],
  providers: [TaskService],
  declarations: [LoaderComponent],
  exports: [LoaderComponent],
})
export class SharedModule {}
