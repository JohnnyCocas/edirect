import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@shared';
import { MaterialModule } from '@app/material.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent, ProjectDialog, TaskDialog } from './home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { I18nModule } from '@app/i18n';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    I18nModule,
    HomeRoutingModule
  ],
  declarations: [HomeComponent, TaskDialog, ProjectDialog],
})
export class HomeModule {}
