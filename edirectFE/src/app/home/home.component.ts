import { Component, Inject, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Project, Task } from '@shared/services/service.interface';
import { ProjectContext, TaskContext, TaskService } from '@shared/services/task.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isLoading = false;
  projectList: Project[] = [];
  projectForm: FormGroup;

  constructor(
    private taskService: TaskService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.isLoading = true;

    this.taskService.getProjectList().pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe((projects) => {
      this.projectList = projects;
      console.log(this.projectList)
    });
  }

  createProject() {
    this.taskService.createProject(this.projectForm.value).pipe(
      finalize(() => {
        this.projectForm.reset();
        this.projectForm.setErrors(null);
        this.isLoading = false;
      })
    ).subscribe((projects) => {
      this.projectList = projects;
    });
  }

  editProject(projectData: ProjectContext, projectId: string) {
    if (projectData) {
      this.taskService.editProject(projectData, projectId).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe((project) => {
        this.projectList.find(p => p.id == projectId).name = project.name;
      });
    }
  }

  deleteProject(projectId: string) {
    this.taskService.deleteProject(projectId).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe((projects) => {
      this.projectList = projects;
    });
  }

  createTask(taskData: TaskContext, projectId: string) {
    if (taskData) {
      this.taskService.createTask(taskData, projectId).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe((project) => {
        this.projectList.find(p => p.id == projectId).tasks = project.tasks;
      });
    }
  }

  editTask(taskData: TaskContext, taskId: string, projectId: string) {
    if (taskData) {
      this.taskService.editTask(taskData, taskId, projectId).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe((project) => {
        this.projectList.find(p => p.id == projectId).tasks = project.tasks;
      });
    }
  }

  toggleTask(taskId: string, projectId: string) {
    this.taskService.toggleTask(taskId, projectId).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe((project) => {
      this.projectList.find(p => p.id == projectId).tasks = project.tasks;
    });
  }

  deleteTask(taskId: string, projectId: string) {
    this.taskService.deleteTask(taskId, projectId).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe((project) => {
      this.projectList.find(p => p.id == projectId).tasks = project.tasks;
    });
  }

  todoList(tasks: Task[] = []) {
    return tasks.filter(t => !t.finished);
  }

  finishedList(tasks: Task[] = []) {
    return tasks.filter(t => t.finished);
  }

  trackById(index: number, obj: any): string {
    return obj.id;
  }

  openDialog(projectId: string, task?: Task): void {
    const dialogRef = this.dialog.open(TaskDialog, {
      width: '250px',
      data: task
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (task) {
          this.editTask(result, task.id, projectId);
        } else {
          this.createTask(result, projectId);
        }
      }
    });
  }

  openProjectDialog(project: Project): void {
    const dialogRef = this.dialog.open(ProjectDialog, {
      width: '250px',
      data: project
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.editProject(result, project.id);
      }
    });
  }

  private createForm() {
    this.projectForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }
}

@Component({
  selector: 'task-dialog',
  templateUrl: './task.dialog.html',
})
export class TaskDialog {

  taskForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<TaskDialog>,
    @Inject(MAT_DIALOG_DATA) public data: TaskContext) {
    this.createForm(data);
  }

  submit(): void {
    this.dialogRef.close(this.taskForm.value);
  }

  get minDate() {
    return new Date();
  }

  private createForm(data: TaskContext) {
    this.taskForm = this.formBuilder.group({
      name: [data?.name ?? '', Validators.required],
      finishDate: [data?.finishDate ?? null]
    });
  }
}

@Component({
  selector: 'project-dialog',
  templateUrl: './project.dialog.html',
})
export class ProjectDialog {

  projectForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<TaskDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ProjectContext) {
    this.createForm(data);
  }

  submit(): void {
    this.dialogRef.close(this.projectForm.value);
  }

  private createForm(data: ProjectContext) {
    this.projectForm = this.formBuilder.group({
      name: [data?.name ?? '', Validators.required]
    });
  }
}
