import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, untilDestroyed } from '@core';
import { AuthenticationService } from './authentication.service';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;
  registerForm!: FormGroup;
  activeForm: FormGroup;
  isLoading = false;
  loginActive = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) {
    this.createForm();
  }

  ngOnInit() {}

  ngOnDestroy() {}

  performAction() {
    this.loginActive ? this.login() : this.register();
  }

  login() {
    this.isLoading = true;
    this.authenticationService.login(this.loginForm.value).pipe(
      finalize(() => {
        this.loginForm.markAsPristine();
        this.isLoading = false;
      }),
      untilDestroyed(this)
    ).subscribe((credentials) => {
      log.debug(`${credentials.username} successfully logged in`);
      this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
    }, (error) => {
      log.debug(`Login error: ${error}`);
      this.error = error;
    });
  }

  register() {
    this.isLoading = true;
    this.authenticationService.register(this.registerForm.value).pipe(
      finalize(() => {
        this.registerForm.markAsPristine();
        this.isLoading = false;
      }),
      untilDestroyed(this)
    ).subscribe((credentials) => {
      this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
    }, (error) => {
      this.error = error;
    });
  }

  toggleRegister() {
    this.loginActive = !this.loginActive;
    this.activeForm = this.loginActive ? this.loginForm : this.registerForm;
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      remember: true
    });
    this.activeForm = this.loginActive ? this.loginForm : this.registerForm;
  }
}
