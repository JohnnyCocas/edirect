import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Credentials, CredentialsService } from './credentials.service';
import { Project } from '@shared/services/service.interface';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '@env/environment';
import { catchError, map } from 'rxjs/operators';

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}
export interface RegisterContext {
  username: string;
  password: string;
  name: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private http: HttpClient,
    private credentialsService: CredentialsService
  ) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    let headers = new HttpHeaders({
      username: context.username,
      password: context.password
    });

    return this.http.get<Credentials>(environment.routes.login(), { headers: headers }).pipe(
      map((creds: Credentials) => {
        if (creds.password) {
          this.credentialsService.setCredentials(creds, context.remember);
        }

        return creds;
      })
    );
  }

  register(context: RegisterContext): Observable<Credentials> {

    return this.http.post<Credentials>(environment.routes.register(), context).pipe(
      map((creds: Credentials) => {
        if (creds.password) {
          this.credentialsService.setCredentials(creds, context.remember);
        }

        return creds;
      })
    );
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }
}
