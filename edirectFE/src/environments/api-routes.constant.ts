export const ROUTES = {

  // Auth
  login: () => `/auth`,
  register: () => `/auth/register`,

  // To-Do List
  getProjectList: () => `/project`,
  createProject: () => `/project`,

  getTaskList: (projId: string) => `/project/${projId}`,
  createTask: (projId: string) => `/project/${projId}`,
  deleteProject: (projId: string) => `/project/${projId}`,
  editProject: (projId: string) => `/project/${projId}`,

  toggleTask: (projId: string, taskId: string) => `/project/${projId}/task/${taskId}`,
  editTask: (projId: string, taskId: string) => `/project/${projId}/task/${taskId}`,
  deleteTask: (projId: string, taskId: string) => `/project/${projId}/task/${taskId}`,
};
